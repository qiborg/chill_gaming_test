# Test task for ChillGaming

## API Documentation

You can also:
  - Add product
  - Get products
  - Get statistic for products

## Authentication
#### Register
##### Request (POST)

URL `/api/register`

Parametres:

| Name | Example |
| ------ | ------ |
| username | artymor |
| password | qwerty12345 |
| email | test@gmail.com |

##### Response (JSON)

Fields:

| Name | Example |
| ------ | ------ |
| token | eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzQwNjczNDJ9.8wja1Tcwq2Ip9gBbCqV8SWjb50p3AdpPVL2syYIHRAQ |

```json
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzQwNjczNDJ9.8wja1Tcwq2Ip9gBbCqV8SWjb50p3AdpPVL2syYIHRAQ"
}
```
#### Login
##### Request (POST)

URL `/api/login`

Parametres:

| Name | Example |
| ------ | ------ |
| username | artymor |
| password | qwerty12345 |
| email | test@gmail.com |

##### Response (JSON)

Fields:

| Name | Example |
| ------ | ------ |
| token | JWToken |

```json
{
  "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJleHAiOjE1NzQwNjczNDJ9.8wja1Tcwq2Ip9gBbCqV8SWjb50p3AdpPVL2syYIHRAQ"
}
```

## Products

`All requests need header "Authorization" consist of JWToken from authentic methods`

#### Add product
##### Request (POST)

`login required`

URL `/add_product`

 - Content-Type: application/json

POST data:

| Name | Example |  Required |
| ------ | ------ | ------ |
| name | text field | true |
| amount | integer field | true |
| price | double precision(2) field | true |

##### Response (JSON)

Fields:

| Name | Example |
| ------ | ------ |
| message | message of status |

```json
{
  "message": "Add product"
}
```


#### Get products
##### Request (GET)

URL `/get_products`

GET data:

| Name | Example | Required |
| ------ | ------ | ------ |
| date_from | timestamp | False |
| dat_to | timestamp | False |
| cost_from | double precision(2) | False |
| cost_to | double precision(2) | False |

##### Response (JSON)

Fields:

| Name | Example |
| ------ | ------ |
| products | list of products |

```json
{
  "products": [
    {
      "name": "item.name",
      "seller": "item.seller.name",
      "cost": "item.cost",
      "amount": "item.amount",
      "created_date": "item.created_date",
    },
    {
      "name": "item.name",
      "seller": "item.seller.name",
      "cost": "item.cost",
      "amount": "item.amount",
      "created_date": "item.created_date",
    },
    { ... },
    ...
  ]
}
```

#### Get statistic products
##### Request (POST)

URL `/stat_products`

  - Content-Type: application/json

POST data:

| Name | Example | Required |
| ------ | ------ | ------ |
| date_from | timestamp | False |
| dat_to | timestamp | False |
| cost_from | double precision(2) | False |
| cost_to | double precision(2) | False |

##### Response (JSON)

```json
{
  "01/12/2019": {
    "products": 7,
    "full_cost": 30000.20,
    "sellers": 5
  },
  "30/11/2019": {
    "products": 5,
    "full_cost": 30050.20,
    "sellers": 5
  },
  "25/11/2019": {
    "products": 7,
    "full_cost": 300.20,
    "sellers": 1
  }

}
```






# Login required

`Method where required auth, you must be insert header`
`Authorization: {token}`
