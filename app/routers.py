import views
import aiohttp_cors

def setup_routers(app):
    app.router.add_route('POST', '/login', views.login)
    app.router.add_route('POST', '/register', views.register)
    
    app.router.add_route('POST', '/add_product', views.add_product)
    app.router.add_route('GET', '/get_products', views.get_products)
    app.router.add_route('POST', '/stat_products', views.stat_products)
