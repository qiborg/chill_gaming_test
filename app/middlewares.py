import jwt
import json
import models
from app import json_response
# JSON Web Token Variables

SECRET = 'secret_phrase'
ALGORITHM = 'HS256'
EXP_DELTA_SECONDS = 5*60


# Middleware function for auth by JWT token

async def jwt_middleware(app, handler):
    async def middleware(request):
        request.user = None
        token = request.headers.get('authorization', None)
        if token:
            try:
                payload = jwt.decode(token,
                                    SECRET,
                                    algorithms = [ALGORITHM])
                print(payload)
            except (jwt.DecodeError, jwt.ExpiredSignatureError):
                return json_response({'message': 'Token is invalid'},
                                     status=400)
            session = models.Session()
            request.user = session.query(models.User).filter_by(id=payload['user_id']).first()
        return await handler(request)
    return middleware
