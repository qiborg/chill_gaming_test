import json
from aiohttp import web
import middlewares
import logging
import routers
import models
from aiohttp_session import session_middleware, SimpleCookieStorage
from aiohttp_middlewares import cors_middleware

log = logging.getLogger(__name__)

def json_response(body='', **kwargs):
    kwargs['body'] = json.dumps(body or kwargs['body']).encode('utf-8')
    kwargs['content_type'] = 'text/json'
    return web.Response(**kwargs)



if __name__ == '__main__':
    models.init_db()
    app = web.Application(middlewares=[middlewares.jwt_middleware, cors_middleware(allow_all=True)])
    routers.setup_routers(app)
    logging.basicConfig(level=logging.DEBUG)

    web.run_app(app)
