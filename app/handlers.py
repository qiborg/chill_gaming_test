from datetime import datetime
import models


def filter_products(data):
    products = Product.query.all()

    if 'date_from' in get_data:
        products = products.filter(
            Product.created_date > datetime.fromtimestamp(get_data['date_from'])
        )
    if 'date_to' in get_data:
        products = products.filter(
            Product.created_date < datetime.fromtimestamp(get_data['date_to'])
        )
    if 'cost_from' in get_data:
        products = products.filter(
            Product.cost > get_data['cost_from']
        )
    if 'cost_to' in get_data:
        products = products.filter(
            Product.cost > get_data['cost_to']
        )

    return products
